-- MySQL dump 10.13  Distrib 5.5.53, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: posts_test
-- ------------------------------------------------------
-- Server version	5.5.53-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (14,'2014_10_12_000000_create_users_table',1),(15,'2016_06_01_000001_create_oauth_auth_codes_table',1),(16,'2016_06_01_000002_create_oauth_access_tokens_table',1),(17,'2016_06_01_000003_create_oauth_refresh_tokens_table',1),(18,'2016_06_01_000004_create_oauth_clients_table',1),(19,'2016_06_01_000005_create_oauth_personal_access_clients_table',1),(20,'2016_12_12_131943_create_posts_table',2),(21,'2016_12_12_183346_create_stats_table',3);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_access_tokens`
--

DROP TABLE IF EXISTS `oauth_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_access_tokens`
--

LOCK TABLES `oauth_access_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_access_tokens` DISABLE KEYS */;
INSERT INTO `oauth_access_tokens` VALUES ('0bf87e06d6062565ab8d79b78bf76589e6eb438dc54096fb562c5d1b29d05f6f1d080ccc33a0cb98',1,3,NULL,'[]',0,'2016-12-12 18:17:28','2016-12-12 18:17:28','2017-12-12 18:17:28'),('355f7548acd965874284b79632acc6917777ded48c9bcf6f8c10b13d93db3a9f7597ff9ca1dbf373',1,3,NULL,'[]',0,'2016-12-12 12:56:57','2016-12-12 12:56:57','2017-12-12 12:56:57'),('482ae35c3538c0899c4bab0a877c94ffaae4a0fbfea06f840cffa8fd21d24b3c3a7653929c41020c',1,3,NULL,'[]',0,'2016-12-12 18:10:21','2016-12-12 18:10:21','2017-12-12 18:10:21'),('52ac81368bd64f4697e393b09731ff192f9b07653d53e664a4c36a7b7e73ace267ff21fd87146677',1,3,NULL,'[]',0,'2016-12-12 12:37:23','2016-12-12 12:37:23','2017-12-12 12:37:23'),('5c810a851355ddef6e801e69320dc68fc2c8664a37d946e8b1325056fe4cce9df7665feec1822fb2',1,3,NULL,'[]',0,'2016-12-12 11:48:11','2016-12-12 11:48:11','2017-12-12 11:48:11'),('6112781739dacc1aec212c5abf13ac23288669d9387cdb6ae3036cc12219382556c23776c6907695',1,3,NULL,'[]',0,'2016-12-12 18:15:03','2016-12-12 18:15:03','2017-12-12 18:15:03'),('7ca5fa14dfbe86e1a1efe029f4c1e1030f7a840e4b98e6a07e5d4118d53e7c133a7a59b62be5da69',1,3,NULL,'[]',0,'2016-12-12 18:15:51','2016-12-12 18:15:51','2017-12-12 18:15:51'),('83829420276e0027e607061ad60b3636732510b12b46e2bc44727b67e2aba4272aa72545e76d2758',1,3,NULL,'[]',0,'2016-12-12 11:42:46','2016-12-12 11:42:46','2017-12-12 11:42:46'),('84724234cf235267be2b8d9a1f4f6e53d7897a88d8d8d204ec7650da9b1fcd0fe455ac4887f5d381',1,3,NULL,'[]',0,'2016-12-12 12:56:51','2016-12-12 12:56:51','2017-12-12 12:56:51'),('8b8941fd3616a3d25b1d2e62d3c06206a2f41ad92e1942ff0e50334db1626c9a170ece2054cd36c6',1,3,NULL,'[]',0,'2016-12-12 18:10:05','2016-12-12 18:10:05','2017-12-12 18:10:05'),('979a46c81ea0264cdf45a2ed43f619881576ce92e0072bea772ca0a69fbdb43ff052b4f4caa31bdf',1,3,NULL,'[]',0,'2016-12-12 17:08:33','2016-12-12 17:08:33','2017-12-12 17:08:33'),('ba728f823aeee8db57f87b852faa658653379bfa907744264dd1b47e834016384ae259f3ad15b602',1,3,NULL,'[]',0,'2016-12-12 18:14:08','2016-12-12 18:14:08','2017-12-12 18:14:08'),('c850100847425f05669c323121096510bb1e0248974b0b95c5029e4ad3295e4071e7d6c5491c4806',1,3,NULL,'[]',0,'2016-12-12 12:56:28','2016-12-12 12:56:28','2017-12-12 12:56:28'),('e514eb0c90c3fc8adc3ffd0506f3ccb1b2c514504b06e86459304d709567ed26d08c08691274bed9',1,3,NULL,'[]',0,'2016-12-12 18:16:07','2016-12-12 18:16:07','2017-12-12 18:16:07'),('ef00450303bebf96a3ad5469abfb7344e7166898391da35db3da7e914c56a28f34c9d1e9c6aadfd3',1,3,NULL,'[]',0,'2016-12-12 12:32:24','2016-12-12 12:32:24','2017-12-12 12:32:24'),('f2ea01bfc22e922a435432cade163b71f1ef9ae3ef5142df4555848df6a91967f36a2f3f7373c047',1,3,NULL,'[]',0,'2016-12-12 18:25:39','2016-12-12 18:25:39','2017-12-12 18:25:39'),('fc4acc69d7e19634625cde08b6478fc65401683036a674337f4ddd8e94cdb176efe4c077f6f91c8e',1,3,NULL,'[]',0,'2016-12-12 19:26:32','2016-12-12 19:26:32','2017-12-12 19:26:32'),('fdb26698cfd166c769130de1c154be6fc39937955e0bd12fe819cea41a3a22a47bc0f0aa282bd9c7',1,3,NULL,'[]',0,'2016-12-12 12:36:47','2016-12-12 12:36:47','2017-12-12 12:36:47');
/*!40000 ALTER TABLE `oauth_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_auth_codes`
--

DROP TABLE IF EXISTS `oauth_auth_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text COLLATE utf8_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_auth_codes`
--

LOCK TABLES `oauth_auth_codes` WRITE;
/*!40000 ALTER TABLE `oauth_auth_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_auth_codes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_clients`
--

DROP TABLE IF EXISTS `oauth_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_clients`
--

LOCK TABLES `oauth_clients` WRITE;
/*!40000 ALTER TABLE `oauth_clients` DISABLE KEYS */;
INSERT INTO `oauth_clients` VALUES (3,NULL,'Posts Test Grant Client','pBc8S07HrwVW0kP485KoQJWwXBoM5QFV0gnqmBCb','http://localhost',0,1,0,'2016-12-12 11:16:23','2016-12-12 11:16:23');
/*!40000 ALTER TABLE `oauth_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_personal_access_clients`
--

DROP TABLE IF EXISTS `oauth_personal_access_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_personal_access_clients_client_id_index` (`client_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_personal_access_clients`
--

LOCK TABLES `oauth_personal_access_clients` WRITE;
/*!40000 ALTER TABLE `oauth_personal_access_clients` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_personal_access_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `oauth_refresh_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_refresh_tokens`
--

LOCK TABLES `oauth_refresh_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_refresh_tokens` DISABLE KEYS */;
INSERT INTO `oauth_refresh_tokens` VALUES ('0b29eca481d0c7aa59206f6a440c462b9caa41d4352d4c8f4f037eb3c79da70c4323ade264b07f28','84724234cf235267be2b8d9a1f4f6e53d7897a88d8d8d204ec7650da9b1fcd0fe455ac4887f5d381',0,'2017-12-12 12:56:51'),('14a4239d78a0dc981820ffd10acf769d33d563325295c0ef2fd6c6bd4db623b3c7ef9649924cd566','6112781739dacc1aec212c5abf13ac23288669d9387cdb6ae3036cc12219382556c23776c6907695',0,'2017-12-12 18:15:03'),('2e4189ba6f8a0e0899e0519a9092910fef8083669cbd23e6690b858807fc01df69970a98959e0d62','83829420276e0027e607061ad60b3636732510b12b46e2bc44727b67e2aba4272aa72545e76d2758',0,'2017-12-12 11:42:46'),('335bb893e677e166f21380992a71edfe791993c5ef01c3b3930ed1911f38daa0550163a1d59ce7c6','f2ea01bfc22e922a435432cade163b71f1ef9ae3ef5142df4555848df6a91967f36a2f3f7373c047',0,'2017-12-12 18:25:39'),('34b695dd0743351f64402b6e1ad4f338f22cd4f166a2f98f68b182b416e52ff48bedb7d6c6b3321c','ba728f823aeee8db57f87b852faa658653379bfa907744264dd1b47e834016384ae259f3ad15b602',0,'2017-12-12 18:14:08'),('550bd8f1ef1236f76c589b24e0388c22dd690bf5fb627d7786bb56e67b5fc8803e4e4c7d285fd5e0','979a46c81ea0264cdf45a2ed43f619881576ce92e0072bea772ca0a69fbdb43ff052b4f4caa31bdf',0,'2017-12-12 17:08:33'),('6761fbd28fa16bf03418de33d63f9469dcc4757718cf20f51bb5651829e10c154b530bf7edc7e644','fc4acc69d7e19634625cde08b6478fc65401683036a674337f4ddd8e94cdb176efe4c077f6f91c8e',0,'2017-12-12 19:26:32'),('6819ad5c94de37d23199e59976651a0550a1a3d807d17559b6892f49b4d029a68f67643974ab7531','5c810a851355ddef6e801e69320dc68fc2c8664a37d946e8b1325056fe4cce9df7665feec1822fb2',0,'2017-12-12 11:48:11'),('69a0a3cfc97d1c8e4f6019347f7ef62e10d76b091c4f8735972a6c368435e96ef2ad05851b23a975','0bf87e06d6062565ab8d79b78bf76589e6eb438dc54096fb562c5d1b29d05f6f1d080ccc33a0cb98',0,'2017-12-12 18:17:28'),('7e98019e8a1a7e460263b349d93fae766992efb8a7b3776139a6624065934a353f1a639acba30ca1','c850100847425f05669c323121096510bb1e0248974b0b95c5029e4ad3295e4071e7d6c5491c4806',0,'2017-12-12 12:56:28'),('8386a9b0bcb07551655961b256378d2ccd89753df6f986075c1f794f9be69a0ba5d0c7b6a197bcb6','52ac81368bd64f4697e393b09731ff192f9b07653d53e664a4c36a7b7e73ace267ff21fd87146677',0,'2017-12-12 12:37:23'),('a3cc513c60e5afa256b0ec30076d15c51fad638cc529dc984cdca13ff1d79418e8ff12afd2ccc068','ef00450303bebf96a3ad5469abfb7344e7166898391da35db3da7e914c56a28f34c9d1e9c6aadfd3',0,'2017-12-12 12:32:24'),('adf5bb7deb397fdba620e5f10df731fad80ca62e76a29f2d85b23093d93679bc87992f61fe12e2ef','482ae35c3538c0899c4bab0a877c94ffaae4a0fbfea06f840cffa8fd21d24b3c3a7653929c41020c',0,'2017-12-12 18:10:21'),('bfeabffd2edf71ba41c01f30eb9826059fb726e4db084a9c652a73697dfadfce14151be14b9c10cd','355f7548acd965874284b79632acc6917777ded48c9bcf6f8c10b13d93db3a9f7597ff9ca1dbf373',0,'2017-12-12 12:56:57'),('d897311a1d282e8f5d76a52c89ffd68e7e5e7d93063be644b3d68ab08eb78fcb4697cec47edcfcee','fdb26698cfd166c769130de1c154be6fc39937955e0bd12fe819cea41a3a22a47bc0f0aa282bd9c7',0,'2017-12-12 12:36:47'),('e3ef234f0175931d3d308b4913ae69b0df37688f9c2f62a322ec7ee2733e6f0c6cb45c53102385db','8b8941fd3616a3d25b1d2e62d3c06206a2f41ad92e1942ff0e50334db1626c9a170ece2054cd36c6',0,'2017-12-12 18:10:05'),('e436e86abfaac5f44087941628b7695213a7781e4839cf671b26b76ebc4fb5d2a0a9927928b6481a','e514eb0c90c3fc8adc3ffd0506f3ccb1b2c514504b06e86459304d709567ed26d08c08691274bed9',0,'2017-12-12 18:16:07'),('fceef3f3e105b636049515bc253eff1e9b63b64cedc386d288bc891ce98c92eb9e53f9f92c72c5ce','7ca5fa14dfbe86e1a1efe029f4c1e1030f7a840e4b98e6a07e5d4118d53e7c133a7a59b62be5da69',0,'2017-12-12 18:15:51');
/*!40000 ALTER TABLE `oauth_refresh_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image_path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `posts_user_id_foreign` (`user_id`),
  CONSTRAINT `posts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (1,1,'Thumbnail label','images/87c4b495344240ddf26333022553873c.jpeg','2016-12-12 17:47:58','2016-12-12 17:47:58'),(2,1,'Thumbnail label','images/782ec53fe748332ce4ca6d1d49d6df15.jpeg','2016-12-12 17:49:34','2016-12-12 17:49:34'),(3,1,'cacamaca post','images/64b77ec062d3bcd263f458d3579565d4.png','2016-12-12 18:19:31','2016-12-12 18:19:31');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stats`
--

DROP TABLE IF EXISTS `stats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stats` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stats`
--

LOCK TABLES `stats` WRITE;
/*!40000 ALTER TABLE `stats` DISABLE KEYS */;
INSERT INTO `stats` VALUES (1,'views','30');
/*!40000 ALTER TABLE `stats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_username_unique` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'user1','$2y$10$R7VvfuCiP1a6hVLPR7TYFO6gyLM1pSJohjq.JuGucykViGjrEmN/i','2016-12-12 11:11:23','2016-12-12 11:11:23'),(2,'user2','$2y$10$ayOSVM9GcaYueJwZLtUG3eje3YZmp9X9ms9EydGU9uxcg5xc4GhlO','2016-12-12 11:11:23','2016-12-12 11:11:23');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-12-12 19:28:29

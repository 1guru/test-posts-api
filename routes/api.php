<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => '/posts'],
    function () {
        Route::get('/', [
                'uses' => 'PostsController@index'
            ]
        );
        Route::post('/', [
                'middleware' => 'auth:api',
                'uses' => 'PostsController@create'
            ]
        );
        Route::get('/{postId}/download-image', [
                'middleware' => 'auth.get',
                'uses' => 'PostsController@downloadImage'
            ]
        );
        Route::get('/download-export', [
                'middleware' => 'auth.get',
                'uses' => 'PostsController@downloadExport'
            ]
        );
    });

Route::group(['prefix' => '/stats'],
    function () {
        Route::get('/', [
                'uses' => 'StatsController@index'
            ]
        );
        Route::get('/increment-views', [
                'uses' => 'StatsController@incrementViews'
            ]
        );
    });
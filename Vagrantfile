# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|

    config.vm.box = "ubuntu/trusty64"
    config.vm.network "private_network", ip: "192.168.33.19"
    config.vm.hostname = "backend-api"
    config.vm.synced_folder ".", "/var/www", :mount_options => ["dmode=777", "fmode=766"]
    config.ssh.insert_key = false
    config.ssh.username = "vagrant"
    config.ssh.password = "vagrant"

    config.vm.provider "virtualbox" do |v|
        v.memory = 2048
        v.cpus = 2
    end

    config.vm.provision "shell", inline: <<-SHELL
        apt-get update
        apt-get install -y language-pack-en-base
        LC_ALL=en_US.UTF-8 add-apt-repository ppa:ondrej/php -y

        apt-get update
        apt-get -y upgrade
        apt-get -y install git zip
        debconf-set-selections <<< 'mysql-server-5.5 mysql-server/root_password password root'
        debconf-set-selections <<< 'mysql-server-5.5 mysql-server/root_password_again password root'
        apt-get -y install mysql-client mysql-server-5.5
        apt-get -y install apache2
        apt-get -y install php7.0 php-apc php7.0-gd php7.0-curl php7.0-intl php7.0-mysql php7.0-cli php7.0-mbstring php-xdebug php7.0-xml php7.0-zip php7.0-mcrypt php7.0-mbstring php7.0-gmp
        apt-get -y install nmap

        php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
        php -r "if (hash_file('SHA384', 'composer-setup.php') === 'aa96f26c2b67226a324c27919f1eb05f21c248b987e6195cad9690d5c1ff713d53020a02ac8c217dbf90a7eacc9d141d') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
        php composer-setup.php
        mv composer.phar /usr/local/bin/composer
        chmod a+x /usr/local/bin/composer

        mysql -uroot -proot -e "CREATE DATABASE posts_test"
        mysql -uroot -proot posts_test < /var/www/database/source.sql


        echo "<VirtualHost *:80>" > /etc/apache2/sites-available/000-default.conf
        echo "      	ServerAdmin webmaster@localhost" >> /etc/apache2/sites-available/000-default.conf
        echo "      	DocumentRoot /var/www/public" >> /etc/apache2/sites-available/000-default.conf
        echo "      	ErrorLog ${APACHE_LOG_DIR}/error.log" >> /etc/apache2/sites-available/000-default.conf
        echo "      	CustomLog ${APACHE_LOG_DIR}/access.log combined" >> /etc/apache2/sites-available/000-default.conf
        echo "      	<Directory /var/www/>" >> /etc/apache2/sites-available/000-default.conf"
        echo "              	Options Indexes FollowSymLinks" >> /etc/apache2/sites-available/000-default.conf
        echo "      	        AllowOverride All" >> /etc/apache2/sites-available/000-default.conf
        echo "              	Require all granted" >> /etc/apache2/sites-available/000-default.conf
        echo "      	</Directory>" >> /etc/apache2/sites-available/000-default.conf"
        echo "      </VirtualHost> >> /etc/apache2/sites-available/000-default.conf

        a2enmod rewrite
        service apache2 restart

        cd /var/www/
        rm -f .env
        ln -s .env.local .env

        composer install
    SHELL

    # Set the timezone to the host timezone
    require 'time'
    timezone = 'Europe/Bucharest' + ((Time.zone_offset(Time.now.zone)/60)/60).to_s
    config.vm.provision :shell, :inline => "if [ $(grep -c UTC /etc/timezone) -gt 0 ]; then echo \"#{timezone}\" | sudo tee /etc/timezone && dpkg-reconfigure --frontend noninteractive tzdata; fi"

end


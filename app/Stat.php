<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stat extends Model
{
    public $table = 'stats';

    public $timestamps = false;

    protected $fillable = [
        'key', 'value'
    ];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public $table = 'posts';

    protected $fillable = [
        'user_id', 'title', 'image_path',
    ];

    public function getImagePathAttribute($value)
    {
        return url($value);
    }
}

<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\Passport;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Rsa\Sha256;
use Lcobucci\JWT\ValidationData;
use League\OAuth2\Server\CryptKey;
use League\OAuth2\Server\CryptTrait;
use League\OAuth2\Server\Exception\OAuthServerException;

class AuthFromGetToken
{

    use CryptTrait;

    public function handle(Request $request, Closure $next, $guard = null)
    {
        if ($request->get('authorization') === false) {
            throw OAuthServerException::accessDenied('Missing "Authorization" header');
        }

        $jwt = $request->get('authorization');

        try {
            // Attempt to parse and validate the JWT
            $token = (new Parser())->parse($jwt);
            if ($token->verify(new Sha256(), (new CryptKey(Passport::keyPath('oauth-public.key')))->getKeyPath()) === false) {
                throw OAuthServerException::accessDenied('Access token could not be verified');
            }

            // Ensure access token hasn't expired
            $data = new ValidationData();
            $data->setCurrentTime(time());

            if ($token->validate($data) === false) {
                throw OAuthServerException::accessDenied('Access token is invalid');
            }

            $accessToken = DB::table('oauth_access_tokens')->find($token->getClaim('jti'));

            // Check if token has been revoked
            if ($accessToken->revoked) {
                throw OAuthServerException::accessDenied('Access token has been revoked');
            }

            $request->merge(['token' => $accessToken]);
            return $next($request);

        } catch (\InvalidArgumentException $exception) {
            // JWT couldn't be parsed so return the request as is
            throw OAuthServerException::accessDenied($exception->getMessage());
        } catch (\RuntimeException $exception) {
            //JWR couldn't be parsed so return the request as is
            throw OAuthServerException::accessDenied('Error while decoding to JSON');
        }
    }
}
<?php

namespace App\Http\Middleware;

use Closure;

class ApiHeaders
{
    public static $headers = [
        'Access-Control-Allow-Credentials' => 'true',
        'Access-Control-Allow-Headers' => 'Origin, X-Requested-With, Content-Type, Accept, Authorization, If-Modified-Since, Cache-Control, Pragma',
        'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS',
    ];

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  string|null $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if ($request->is('api/*') || $request->is('oauth/*')) {
            // ALLOW OPTIONS METHOD
            $response = $next($request);

            foreach (self::$headers as $key => $value) {
                header("{$key}: {$value}");
            }
            header("Access-Control-Allow-Origin: " . env('WEB_URL'));

            return $response;
        }
        return $next($request);
    }
}

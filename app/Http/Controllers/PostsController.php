<?php

namespace App\Http\Controllers;

use App\Post;
use App\User;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class PostsController extends Controller
{

    public function index()
    {
        return Post::orderBy('created_at', 'DESC')->get();
    }

    public function create(Request $request)
    {
        $path = $request->file('file')->store('images');

        $full_path = storage_path() . '/app/' . $path;

        $img = Image::make($full_path)->resize(300, 200);
        $img->save(public_path($path), 80);

        Post::create([
            'user_id' => $request->user()->id,
            'title' => $request->get('title', ''),
            'image_path' => $path
        ]);

        return response([], 201);
    }

    public function downloadImage(Request $request, $postId)
    {
        $userId = $request->get('token')->user_id;

        /**
         * @var $user User
         */
        $user = User::findOrFail($userId);

        $post = Post::findOrFail($postId);

        $path = str_replace(url('/'), '', $post->image_path);
        return response()->download(storage_path() . '/app/' . $path);
    }

    public function downloadExport(Request $request)
    {
        $userId = $request->get('token')->user_id;

        /**
         * @var $user User
         */
        $user = User::findOrFail($userId);

        $posts = Post::all();

        $file = storage_path() . "/app/posts.csv";
        $fh = fopen($file, 'w');

        if (!$fh) return false;

        fputcsv($fh, ['Title', 'Filename']);
        foreach ($posts as $post) {
            fputcsv($fh, [$post->title, $post->image_path]);
        }
        fclose($fh);

        exec('/usr/bin/zip ' . storage_path('app') . '/images.zip ' . storage_path('app/images/') . '* ' . storage_path('app/posts.csv'), $output);
        return response()->download(storage_path() . '/app/images.zip');
    }
}
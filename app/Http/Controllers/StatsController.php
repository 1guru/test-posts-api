<?php

namespace App\Http\Controllers;

use App\Post;
use App\Stat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;

class StatsController extends Controller
{

    public function index()
    {
        return [
            'posts' => Post::count(),
            'views' => Stat::where('key', 'views')->value('value')
        ];
    }

    public function incrementViews()
    {
        $views = Stat::where('key', 'views')->first();
        if(!$views) {
            $views = Stat::create([
               'key' => 'views',
                'value' => 0
            ]);
        }
        $views = Stat::where('key', 'views')->increment('value');
    }

}